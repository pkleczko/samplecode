package com.interview.task.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.interview.task.R;
import com.interview.task.adapters.TaskStickyListAdapter;
import com.interview.task.helpers.TaskDialog;
import com.interview.task.model.TaskGroup;
import com.interview.task.model.TaskItem;
import com.interview.task.utils.JsonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class TaskFragment extends Fragment implements StickyListHeadersListView.OnHeaderClickListener, AdapterView.OnItemLongClickListener, TaskDialog.TaskDialogListener, StickyListHeadersListView.OnHeaderLongClickListener {

    private static final String DATAFILE = "data.json";
    //private static final String DATAFILE = "data2.json";

    private OnFragmentInteractionListener mListener;
    private ExpandableStickyListHeadersListView expandableStickyList;
    private ProgressDialog progressDialog;
    private AlertDialog itemDetailsDialog;
    private HashMap<Integer, String> groupsMap;
    private boolean headerLongClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task, null);

        expandableStickyList = (ExpandableStickyListHeadersListView) view.findViewById(R.id.list);

        getList();

        return view;
    }

    private void getList() {
        //load assets asynchronously
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                return JsonUtils.loadJSONFromAsset(getActivity().getApplicationContext(), params[0]);
            }

            @Override
            protected void onPostExecute(String json) {
                dismissProgressDialog();
                setList(json);
            }
        };

        showProgressDialog("Loading..");
        task.execute(DATAFILE);
    }

    private void setList(String json) {
        //initiate list elements
        groupsMap = new HashMap<Integer, String>(); //we need to store this Map to display group name when on item long click
        ArrayList<TaskItem> items = new ArrayList<TaskItem>();

        if (!TextUtils.isEmpty(json)) {
            //parse with Gson lib
            final Gson gson = new Gson();

            final JsonElement jElement = new JsonParser().parse(json);
            final JsonObject jObject = jElement.getAsJsonObject();

            //create groups Map for quick group attributes access in adapter (only String in our case but could have more)
            final JsonArray jGroups = jObject.getAsJsonArray("groups");
            for (JsonElement jGroupElement : jGroups) {
                groupsMap.put(jGroupElement.getAsJsonObject().get("id").getAsInt(),jGroupElement.getAsJsonObject().get("name").getAsString());
            }
            //we need to create groups list only for sorting
            final ArrayList<TaskGroup> groups = gson.fromJson(jGroups, new TypeToken<List<TaskGroup>>() { }.getType());

            //create TaskItem list
            final JsonArray jItems = jObject.getAsJsonArray("items");
            items = gson.fromJson(jItems, new TypeToken<List<TaskItem>>() { }.getType());

            //sort items by groups name
            sortItems(groups, items);
        }

        //finally set adapter
        StickyListHeadersAdapter adapter = new TaskStickyListAdapter(getActivity().getApplicationContext(), items, groupsMap);
        expandableStickyList.setAdapter(adapter);

        //collapse groups
        collapseAllHeaders();
    }

    private void collapseAllHeaders() {
        for (int i = 0; i < expandableStickyList.getCount();i++) {
            expandableStickyList.collapse(expandableStickyList.getAdapter().getHeaderId(i));
        }
    }

    private void sortItems(ArrayList<TaskGroup> groups, ArrayList<TaskItem> items) {
        //firstly sort groups by name
        Collections.sort(groups);
        ArrayList<Integer> sortedGroups = new ArrayList<Integer>(groups.size());
        for (TaskGroup g : groups) {
            sortedGroups.add(g.getId());
        }

        //we have additional field in TaskItem that tells current groups sort order
        for (TaskItem i : items) {
            i.setSortOrder(sortedGroups.indexOf(i.getGroupID()));
        }
        //finally we sort by this additional field
        Collections.sort(items);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        expandableStickyList.setOnHeaderClickListener(this);
        expandableStickyList.setOnItemLongClickListener(this);
        expandableStickyList.setOnHeaderLongClickListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        expandableStickyList.setOnHeaderClickListener(null);
        expandableStickyList.setOnItemLongClickListener(null);
        expandableStickyList.setOnHeaderLongClickListener(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
            if (expandableStickyList.isHeaderCollapsed(headerId)) {
                expandableStickyList.expand(headerId);
            } else {
                expandableStickyList.collapse(headerId);
            }
    }



    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (!headerLongClicked) {
            TaskItem item = ((TaskItem)expandableStickyList.getItemAtPosition(position));
        itemDetailsDialog = new TaskDialog(this).
                create(getActivity(), groupsMap.get(item.getGroupID()), item.getName(), null, "OK");
        itemDetailsDialog.show();
        }
        headerLongClicked = false;
        return true;
    }

    @Override
    public void onPositiveButtonClick() {
        itemDetailsDialog.dismiss();
    }

    @Override
    public void onNegativeButtonClick() {

    }

    @Override
    public void onHeaderLongClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
        headerLongClicked = true;

        Animation rotate = null;
        if ((headerId % 2) == 0)
            rotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate_right);
        else
            rotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate_left);

        expandableStickyList.startAnimation(rotate);
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public void showProgressDialog(final String text) {
        progressDialog = ProgressDialog.show(getActivity(), "", text);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(final DialogInterface dialog) {
                dismissProgressDialog();
            }
        });
    }

    public void dismissProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog.setOnCancelListener(null);
            }
        } catch (final IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}
