package com.interview.task;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Paweł on 2015-01-28.
 */
public class TaskApplication extends Application {

    private static TaskApplication singleton;

    public TaskApplication getInstance(){
        return singleton;
    }

    public Typeface roboto_light;

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        roboto_light = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
    }

}
