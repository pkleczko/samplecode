package com.interview.task.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Paweł on 2015-01-28.
 */
public class JsonUtils {

    public static String loadJSONFromAsset(Context context, String filename) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
