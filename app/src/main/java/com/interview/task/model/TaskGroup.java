package com.interview.task.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Paweł on 2015-01-28.
 */
public class TaskGroup implements Serializable, Comparable {

    private static final long serialVersionUID = -3455251790995702077L;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Object another) {
        return getName().compareTo(((TaskGroup)another).getName());
    }
}
