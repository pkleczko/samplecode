package com.interview.task.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Paweł on 2015-01-28.
 */
public class TaskItem implements Serializable, Comparable {

    private static final long serialVersionUID = -7903312579662894610L;

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("group_id")
    @Expose
    private Integer groupID;

    @SerializedName("name")
    @Expose
    private String name;

    private int sortOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int order) {
        this.sortOrder = order;
    }

    @Override
    public int compareTo(Object another) {
        if (getSortOrder() > ((TaskItem)another).getSortOrder()) {
            return 1;
        }
        else if (getSortOrder() < ((TaskItem)another).getSortOrder()) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
