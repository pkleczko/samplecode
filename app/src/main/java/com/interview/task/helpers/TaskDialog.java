package com.interview.task.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.interview.task.R;
import com.interview.task.TaskApplication;

/**
 * Created by Paweł on 2015-01-28.
 */
public class TaskDialog  implements View.OnClickListener {

    TaskDialogListener listener;
    Button positive;
    Button negative;

    public TaskDialog(TaskDialogListener listener) {
        this.listener = listener;
    }

    public AlertDialog create(Activity activity) {
        return create(activity, "Błąd", "Brak połączenia z Internetem", "Anuluj", "Ponów próbę");
    }

    public AlertDialog create(Activity activity, String description, String cancel, String ok) {
        return create(activity, "Błąd", description, cancel, ok);
    }

    public AlertDialog create(Activity activity, String titleText, String descriptionText, String cancelText, String okText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        View layout = LayoutInflater.from(activity).inflate(R.layout.dialog_task, null);

        TaskApplication application = ((TaskApplication) activity.getApplication()).getInstance();

        TextView title = (TextView) layout.findViewById(R.id.title);
        TextView description = (TextView) layout.findViewById(R.id.description);

        title.setText(titleText);
        description.setText(descriptionText);
        title.setTypeface(application.roboto_light);
        description.setTypeface(application.roboto_light);

        positive = (Button) layout.findViewById(R.id.ok);
        positive.setOnClickListener(this);
        positive.setTypeface(application.roboto_light);
        positive.setText(okText);

        if (!TextUtils.isEmpty(cancelText)) {
            negative = (Button) layout.findViewById(R.id.cancel);
            negative.setOnClickListener(this);
            negative.setTypeface(application.roboto_light);
            negative.setText(cancelText);
        } else {
            negative = (Button) layout.findViewById(R.id.cancel);
            negative.setVisibility(View.GONE);
        }

        builder.setView(layout);
        return builder.create();
    }

    public interface TaskDialogListener {
        public void onPositiveButtonClick();
        public void onNegativeButtonClick();
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==positive.getId()) {
            listener.onPositiveButtonClick();
        } else if (view.getId()==negative.getId()) {
            listener.onNegativeButtonClick();
        }
    }
}
