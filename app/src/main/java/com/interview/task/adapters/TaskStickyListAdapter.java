package com.interview.task.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.interview.task.R;
import com.interview.task.TaskApplication;
import com.interview.task.model.TaskItem;

import java.util.ArrayList;
import java.util.HashMap;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Paweł on 2015-01-28.
 */
public class TaskStickyListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private final Context mContext;
    private final LayoutInflater mInflater;
    private final TaskApplication mApplication;
    private final ArrayList<TaskItem> mItems;
    private final HashMap<Integer, String> mGroups;

    public TaskStickyListAdapter(Context context, ArrayList<TaskItem> items, HashMap<Integer,String> groups) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mApplication = ((TaskApplication) context).getInstance();
        this.mItems = items;
        this.mGroups = groups;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder headerViewHolder;

        if (convertView==null) {
            headerViewHolder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.list_task_header, parent, false);
            headerViewHolder.name = (TextView) convertView.findViewById(R.id.header);

            headerViewHolder.name.setTypeface(mApplication.roboto_light);

            convertView.setTag(headerViewHolder);
        } else
            headerViewHolder = (HeaderViewHolder) convertView.getTag();

        //check nulls if assets source not confident - for now it's useless
        headerViewHolder.name.setText(getHeaderName(getItem(position).getGroupID()));

        return convertView;
    }

    private String getHeaderName(Integer groupID) {
        return mGroups.get(groupID);
    }

    @Override
    public long getHeaderId(int position) {
        return getItem(position).getGroupID();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public TaskItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_task_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.item);

            viewHolder.name.setTypeface(mApplication.roboto_light);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(getItem(position).getName());

        return convertView;
    }

    private class HeaderViewHolder {
        TextView name;
    }

    private class ViewHolder {
        TextView name;
    }
}
